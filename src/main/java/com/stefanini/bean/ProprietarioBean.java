package com.stefanini.bean;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.stefanini.model.Proprietario;
import com.stefanini.model.Telefone;
import com.stefanini.model.TipoTelefone;
import com.stefanini.service.ProprietarioService;
import com.stefanini.service.TipoTelefoneService;

@Named("proprietarioMB")
@SessionScoped
public class ProprietarioBean implements Serializable {

	private static final long serialVersionUID = -2157009794256537118L;

	@Inject
    private ProprietarioService proprietarioService;
	
	@Inject
	private TipoTelefoneService tipoTelefoneService;
	
	@Inject
	private Proprietario proprietario;
	
	@Inject
	private Telefone telefone;
	
	private List<Proprietario> proprietarios;
	
	private List<TipoTelefone> tiposTelefone;
	
	private boolean novoProprietario = false;
    
	@PostConstruct
	public void init() {
		tiposTelefone = tipoTelefoneService.getTodosTiposTelefone();
		proprietarios = proprietarioService.getTodosProprietario();
	}
	
	public String chamar() {
        return "/pages/teste.faces?faces-redirect=true";
    }
	
	public String exibirCadastroProprietario() {
        return "/proprietario/listarProprietario.faces?faces-redirect=true";
    }
	
	public String novoProprietario() {
		novoProprietario = true;
	
		setProprietario(new Proprietario());
		setTelefone(new Telefone());
		
		return "/proprietario/editarProprietario.xhtml?faces-redirect=true";
	}
	
	public String exibirProprietario(Proprietario proprietario) {
		novoProprietario = false;
		Proprietario p = proprietarioService.getProprietario(proprietario);
		setProprietario(p);
		setTelefone(new Telefone());
		
		return "/proprietario/editarProprietario.xhtml?faces-redirect=true";
	}
	
	public String salvarProprietario() {
		try {
			if(novoProprietario) {
				proprietarioService.incluir(getProprietario());
			} else {
				proprietarioService.alterar(getProprietario());
			}
			
			proprietarios = proprietarioService.getTodosProprietario();
			
			return "/proprietario/listarProprietario.xhtml?faces-redirect=true";
		} catch (Exception e) {
			return null;
		}
	}
	
	public void removerProprietario(Proprietario proprietario) {
		try {
			proprietarioService.excluir(proprietario);
			proprietarios = proprietarioService.getTodosProprietario();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void adicionarTelefone() {
		Proprietario p = getProprietario();
		
		if(getTelefone().getDdd() == null || getTelefone().getNumero() == null) {
			return;
		}
		
		if(p.getTelefones() == null) {
			p.setTelefones(new ArrayList<Telefone>());
		}
		
		if(!p.getTelefones().contains(getTelefone())) {
			p.getTelefones().add(getTelefone());
		}
		
		setTelefone(new Telefone());
	}
	
	public String voltarLista() {
		return "/proprietario/listarProprietario.xhtml?faces-redirect=true";
	}
	
	public void editarTelefone(Telefone telefone) {
		this.telefone = telefone;
	}
	
	public void excluirTelefone(Telefone telefone) {
		getProprietario().getTelefones().remove(telefone);
	}
	
	public Proprietario getProprietario() {
		return proprietario;
	}

	public void setProprietario(Proprietario proprietario) {
		this.proprietario = proprietario;
	}
	
	public Telefone getTelefone() {
		return telefone;
	}

	public void setTelefone(Telefone telefone) {
		this.telefone = telefone;
	}
	
	public List<Proprietario> getProprietarios() {
		return proprietarios;
	}

	public void setProprietarios(List<Proprietario> proprietarios) {
		this.proprietarios = proprietarios;
	}

	public List<TipoTelefone> getTiposTelefone() {
		return tiposTelefone;
	}

	public void setTiposTelefone(List<TipoTelefone> tiposTelefone) {
		this.tiposTelefone = tiposTelefone;
	}
}
