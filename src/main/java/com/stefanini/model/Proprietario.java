package com.stefanini.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Proprietario implements Serializable {

	private static final long serialVersionUID = 186586564095030146L;

	@Id
    private Long cpfProprietario;
    
    private String nome;
    
    @ElementCollection
    @OneToMany(cascade=CascadeType.ALL, mappedBy="proprietario", orphanRemoval=true, fetch=FetchType.EAGER)
    private List<Telefone> telefones;

	public Long getCpfProprietario() {
		return cpfProprietario;
	}

	public void setCpfProprietario(Long cpfProprietario) {
		this.cpfProprietario = cpfProprietario;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Telefone> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<Telefone> telefones) {
		this.telefones = telefones;
	}
}
