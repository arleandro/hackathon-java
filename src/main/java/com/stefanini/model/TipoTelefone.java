package com.stefanini.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class TipoTelefone implements Serializable {
	private static final long serialVersionUID = -232575076290519475L;
	
	@Id
	private Integer idTipoTelefone;
	private String descricaoTipoTelefone;
	
	public Integer getIdTipoTelefone() {
		return idTipoTelefone;
	}
	
	public void setIdTipoTelefone(Integer idTipoTelefone) {
		this.idTipoTelefone = idTipoTelefone;
	}
	
	public String getDescricaoTipoTelefone() {
		return descricaoTipoTelefone;
	}
	
	public void setDescricaoTipoTelefone(String descricaoTipoTelefone) {
		this.descricaoTipoTelefone = descricaoTipoTelefone;
	}
}
