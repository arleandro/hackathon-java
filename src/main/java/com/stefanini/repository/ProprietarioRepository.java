package com.stefanini.repository;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.stefanini.model.Proprietario;
import com.stefanini.model.Telefone;

public class ProprietarioRepository {

	@Inject
	private EntityManager manager;
	
	public Proprietario getProprietario(Proprietario proprietario) {
		return this.manager.find(Proprietario.class, proprietario.getCpfProprietario());
	}

	@SuppressWarnings("unchecked")
	public List<Proprietario> getTodosProprietario() {
		return this.manager.createQuery("select p from Proprietario p").getResultList();
	}

	public void incluir(Proprietario proprietario) {
		List<Telefone> list = proprietario.getTelefones();
		
		if(list != null) {
			for (Telefone telefone : list) {
				telefone.setProprietario(proprietario);
			}
		}
		
		this.manager.persist(proprietario);
	}

	public void alterar(Proprietario proprietario) {
		Proprietario p = getProprietario(proprietario);
		
		p.getTelefones().clear();
		
		this.manager.flush();
		
		List<Telefone> list = proprietario.getTelefones();
		
		if(list != null) {
			for (Telefone telefone : list) {
				telefone.setProprietario(p);
			}
		}
		
		p.setTelefones(proprietario.getTelefones());	
		
		p.setNome(proprietario.getNome());
		
		this.manager.merge(p);
	}
	
	public void excluir(Proprietario proprietario) {
		Proprietario p = getProprietario(proprietario);
		
		this.manager.remove(p);
	}
}




