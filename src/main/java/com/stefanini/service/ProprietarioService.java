package com.stefanini.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.stefanini.model.Proprietario;
import com.stefanini.repository.ProprietarioRepository;

@Stateless
public class ProprietarioService {

    @Inject
    private ProprietarioRepository proprietarioRepository;

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Proprietario getProprietario(Proprietario proprietario){
    	return proprietarioRepository.getProprietario(proprietario);
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Proprietario> getTodosProprietario() {
		return proprietarioRepository.getTodosProprietario();
	}
	
    public void incluir(Proprietario proprietario) {
    	proprietarioRepository.incluir(proprietario);
    }

    public void alterar(Proprietario proprietario) {
    	proprietarioRepository.alterar(proprietario);
    }

    public void excluir(Proprietario proprietario) {
    	proprietarioRepository.excluir(proprietario);
    }
}
