/**
 * 
 */
package com.stefanini.ws.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.stefanini.model.Proprietario;
import com.stefanini.model.Telefone;
import com.stefanini.service.ProprietarioService;

/**
 * @author aamaciel
 *
 */
@RequestScoped
@Path("/proprietario")
@Produces({ "application/xml", "application/json" })
@Consumes({ "application/xml", "application/json" })
public class ProprietarioResource {
	
	@Inject
    private ProprietarioService proprietarioService;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public List<Proprietario> pesquisarFUncionario(@QueryParam("cpfProprietario") Long cpfProprietario) {
		List<Proprietario> retorno = new ArrayList<Proprietario>();
		
		Proprietario proprietario = new Proprietario();
		proprietario.setCpfProprietario(cpfProprietario);
		
		proprietario = proprietarioService.getProprietario(proprietario);
		
		retorno.add(getProprietario(proprietario));
		
		return retorno;
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Boolean incluirFuncionario(Proprietario proprietario) {
		Iterator<Telefone> it = proprietario.getTelefones().iterator();
		
		while (it.hasNext()) {
			Telefone telefone = (Telefone) it.next();
			telefone.setProprietario(proprietario);
		}
		
		proprietarioService.incluir(proprietario);
		
		return true;
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Boolean alterarFuncionario(Proprietario proprietario) {
		Iterator<Telefone> it = proprietario.getTelefones().iterator();
		
		while (it.hasNext()) {
			Telefone telefone = (Telefone) it.next();
			telefone.setProprietario(proprietario);
		}
		
		proprietarioService.alterar(proprietario);
		
		return true;
	}

	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Boolean excluirFuncionario(Proprietario proprietario) {
		proprietarioService.excluir(proprietario);
		
		return true;
	}
	
	private Proprietario getProprietario(Proprietario proprietario) {
		Proprietario retorno = new Proprietario();
		
		retorno.setCpfProprietario(proprietario.getCpfProprietario());
		retorno.setNome(proprietario.getNome());
		
		List<Telefone> list = new ArrayList<Telefone>();
		
		for (Telefone telefone : proprietario.getTelefones()) {
			Telefone fone = new Telefone();
			fone.setDdd(telefone.getDdd());
			fone.setNumero(telefone.getNumero());
			fone.setTipoTelefone(telefone.getTipoTelefone());
			list.add(fone);
		}
		
		retorno.setTelefones(list);
		
		return retorno;
	}
}
