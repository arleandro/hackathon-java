package com.stefanini.ws.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * @author aamaciel
 *
 */
@ApplicationPath("/resources")
public class RestApplication extends Application {

}
